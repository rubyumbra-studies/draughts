# Draughts project

Implementation of game Draughts. Main functions:

1) <b>Online mode</b> with other player - simply create game with other person and play. There's a standard rule of compulsore jumping if it's possible.

2) <b>Train mode</b> - want to try some game-changing strategy? Let's try to do in <b>Train mode</b> firstly, where you can play with yourself to check your strategy.

3) <b>Import game</b> from Lidraughts.org - found wonderful non-finished game on the website and think you could finish it in other way? Try it out in <b>Import mode</b>

Project are written with Angular + Kotlin and wrapped in Docker. To build it locally just run <b>docker-compose up</b> and navigate to <b>localhost:4200</b> to try it out. 

<b>Good luck and have fun!</b>
