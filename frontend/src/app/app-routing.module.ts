import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChessBoardComponent } from './game/chess-board/chess-board.component';
import { MenuListComponent } from './menu/menu-list/menu-list.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { LoginActivate } from './services/login-activate.guard';
import { GamesListComponent } from './menu/games-list/games-list.component';

const routes: Routes = [
  {
    path: 'train',
    component: ChessBoardComponent,
    pathMatch: 'full',
    canActivate: [LoginActivate],
  },
  {
    path: 'game/:gameId',
    component: ChessBoardComponent,
    canActivate: [LoginActivate],
  },
  { path: 'menu', component: MenuListComponent, canActivate: [LoginActivate] },
  {
    path: 'list-games',
    component: GamesListComponent,
    canActivate: [LoginActivate],
    pathMatch: 'full',
  },
  { path: 'login', component: LoginPageComponent },
  { path: '**', redirectTo: '/menu' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
