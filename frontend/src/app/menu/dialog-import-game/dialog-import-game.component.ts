import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map, Observable, startWith, Subject, takeUntil } from 'rxjs';
import { FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { User } from '../../interfaces';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-import-game',
  templateUrl: './dialog-import-game.component.html',
  styleUrls: ['./dialog-import-game.component.less'],
})
export class DialogImportGameComponent implements OnInit, OnDestroy {
  public users$: Observable<User[]> | null = null;
  public colors = ['white', 'black'];
  public selectForm = new FormControl();
  public idForm = new FormControl();
  public colorForm = new FormControl();

  private destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private http: HttpClient,
    private authService: AuthService,
    private dialogRef: MatDialogRef<DialogImportGameComponent>
  ) {}

  ngOnInit(): void {
    this.users$ = this.http.get<User[]>('/users').pipe(
      startWith([]),
      takeUntil(this.destroy$),
      map((users: User[]) =>
        users.filter((user) => user.username !== this.authService.userName)
      )
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSubmit(): void {
    this.dialogRef.close({
      name: this.selectForm.value,
      id: this.idForm.value,
      color: this.colorForm.value,
    });
  }
}
