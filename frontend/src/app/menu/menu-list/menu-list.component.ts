import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogNewGameComponent } from '../dialog-new-game/dialog-new-game.component';
import { HttpClient } from '@angular/common/http';
import { map, switchMap, take } from 'rxjs';
import { Game } from '../../interfaces';
import { DialogImportGameComponent } from '../dialog-import-game/dialog-import-game.component';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.less'],
})
export class MenuListComponent {
  constructor(
    private router: Router,
    private dialog: MatDialog,
    private http: HttpClient,
    private authService: AuthService
  ) {}

  onClickCreateNewGame(): void {
    this.dialog
      .open(DialogNewGameComponent)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.createNewGame(result);
        }
      });
  }

  onClickImportGame(): void {
    this.dialog
      .open(DialogImportGameComponent)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.importGame(result.id, result.name, result.color);
        }
      });
  }

  onClickActiveGames(): void {
    this.router.navigate(['/list-games']);
  }

  createTrain(): void {
    this.router.navigate(['/train']);
  }

  private createNewGame(opponent: string): void {
    this.http
      .post(`/games/create/${opponent}`, {})
      .pipe(
        take(1),
        switchMap((gameId) =>
          this.http.get<Game[]>('/games').pipe(
            take(1),
            map((games: Game[]) =>
              games.find((game: Game) => game.id == gameId)
            )
          )
        )
      )
      .subscribe((game: Game | undefined) => {
        if (game) {
          this.router.navigate(['/game', game.id], {
            queryParams: {
              white: game.whiteId,
              black: game.blackId,
            },
          });
        }
      });
  }

  private importGame(id: string, opponent: string, color: 'white' | 'black'): void {
    this.http
      .post(`/import`, {
        id,
        white: color === 'white' ? this.authService.userName : opponent,
        black: color === 'black' ? this.authService.userName : opponent,
      })
      .pipe(
        take(1),
        switchMap((gameId) =>
          this.http.get<Game[]>('/games').pipe(
            take(1),
            map((games: Game[]) =>
              games.find((game: Game) => game.id == gameId)
            )
          )
        )
      )
      .subscribe((game: Game | undefined) => {
        if (game) {
          this.router.navigate(['/game', game.id], {
            queryParams: {
              white: game.whiteId,
              black: game.blackId,
            },
          });
        }
      });
  }
}
