import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {
  combineLatest,
  map,
  Observable,
  startWith,
  Subject,
  takeUntil,
} from 'rxjs';
import { FormControl } from '@angular/forms';
import { Game, User } from '../../interfaces';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.less'],
})
export class GamesListComponent implements OnInit, OnDestroy {
  public games$: Observable<Game[]> | null = null;
  public selectForm = new FormControl();

  private destroy$ = new Subject<void>();

  constructor(private router: Router, private http: HttpClient) {}

  getFormattedDate(timestamp: string): string {
    return new Date(timestamp + 'Z').toLocaleString();
  }

  ngOnInit(): void {
    this.games$ = combineLatest([this.getGames$(), this.getUsers$()]).pipe(
      map(([games, users]) =>
        games.map((game) => ({
          ...game,
          whiteUsername: users.find((user) => user.id === game.whiteId)
            ?.username,
          blackUsername: users.find((user) => user.id === game.blackId)
            ?.username,
        }))
      )
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onGameClick(gameId: number, whiteId: number, blackId: number): void {
    this.router.navigate(['/game', gameId], {
      queryParams: {
        white: whiteId,
        black: blackId,
      },
    });
  }

  private getGames$(): Observable<Game[]> {
    return this.http
      .get<Game[]>('/games')
      .pipe(startWith([]), takeUntil(this.destroy$));
  }

  private getUsers$(): Observable<User[]> {
    return this.http
      .get<User[]>('/users')
      .pipe(startWith([]), takeUntil(this.destroy$));
  }
}
