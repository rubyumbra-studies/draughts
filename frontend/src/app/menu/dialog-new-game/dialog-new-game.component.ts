import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map, Observable, startWith, Subject, takeUntil } from 'rxjs';
import { FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { User } from '../../interfaces';

@Component({
  selector: 'app-dialog-new-game',
  templateUrl: './dialog-new-game.component.html',
  styleUrls: ['./dialog-new-game.component.less'],
})
export class DialogNewGameComponent implements OnInit, OnDestroy {
  public users$: Observable<User[]> | null = null;
  public selectForm = new FormControl();

  private destroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private http: HttpClient,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.users$ = this.http.get<User[]>('/users').pipe(
      startWith([]),
      takeUntil(this.destroy$),
      map((users: User[]) =>
        users.filter((user) => user.username !== this.authService.userName)
      )
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
