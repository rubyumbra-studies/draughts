import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { MenuListComponent } from './menu-list/menu-list.component';
import { DialogNewGameComponent } from './dialog-new-game/dialog-new-game.component';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { GamesListComponent } from './games-list/games-list.component';
import { DialogImportGameComponent } from './dialog-import-game/dialog-import-game.component';
import { MatInputModule } from '@angular/material/input';

const COMPONENTS = [
  MenuListComponent,
  DialogNewGameComponent,
  GamesListComponent,
  DialogImportGameComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
  ],
  exports: [...COMPONENTS],
})
export class MenuModule {}
