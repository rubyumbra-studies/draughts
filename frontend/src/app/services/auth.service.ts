import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, shareReplay, switchMap, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../interfaces';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of } from 'rxjs';

const LOGGED_IN_KEY = 'login';
const USER_NAME_KEY = 'userName';
const USER_ID_KEY = 'userId';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  get isLoggedIn(): boolean {
    return !!this.getCookie(LOGGED_IN_KEY);
  }

  get loginCookie(): string | null {
    return this.getCookie(LOGGED_IN_KEY);
  }

  get userName(): string {
    return this.getCookie(USER_NAME_KEY) ?? '';
  }

  get userId(): number {
    return parseInt(this.getCookie(USER_ID_KEY) ?? '0');
  }

  register(login: string, password: string) {
    return this.http.post('/auth/register', { login, password }).pipe(
      shareReplay(1),
      tap(() => {
        this.setCookie(LOGGED_IN_KEY, btoa(`${login}:${password}`), 5);
        this.setCookie(USER_NAME_KEY, login, 5);
      }),
      switchMap(() => this.http.get<User>(`/users/${login}`)),
      tap((user: User) => this.setCookie(USER_ID_KEY, user.id.toString(), 5))
    );
  }

  login(login: string, password: string) {
    return this.http.post(`/auth/login`, { login, password }).pipe(
      shareReplay(1),
      tap(() => {
        this.setCookie(LOGGED_IN_KEY, btoa(`${login}:${password}`), 5);
        this.setCookie(USER_NAME_KEY, login, 5);
      }),
      switchMap(() => this.http.get<User>(`/users/${login}`)),
      tap((user: User) => this.setCookie(USER_ID_KEY, user.id.toString(), 5)),
      catchError(() => {
        this.snackBar.open(
          'The login or password is incorrect. Please try again.',
          '',
          {
            duration: 2000,
          }
        );

        return of();
      })
    );
  }

  logout(): void {
    this.eraseCookie(LOGGED_IN_KEY);
    this.getCookie(LOGGED_IN_KEY);
    this.eraseCookie(USER_NAME_KEY);
    this.eraseCookie(USER_ID_KEY);

    this.router.navigateByUrl('/login');
  }

  private setCookie(name: string, value: string, days?: number): void {
    let expires = '';

    if (days) {
      const date = new Date();

      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = '; expires=' + date.toUTCString();
    }
    document.cookie = name + '=' + (value || '') + expires + '; path=/';
  }

  private getCookie(name: string): string | null {
    const nameEQ = name + '=';
    const ca = document.cookie.split(';');

    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];

      while (c.charAt(0) == ' ') {
        c = c.substring(1, c.length);
      }

      if (c.indexOf(nameEQ) == 0) {
        return c.substring(nameEQ.length, c.length);
      }
    }

    return null;
  }

  private eraseCookie(name: string) {
    document.cookie = name + '=; Max-Age=-99999999;';
  }
}
