export interface Action {
  gameId: number;
  timestamp: string;
  fromX: number;
  fromY: number;
  toX: number;
  toY: number;
  id?: number;
  lastTurn: boolean;
}
