export interface Game {
  id: number;
  whiteId: number;
  blackId: number;
  timestamp: string;
  whiteUsername?: string;
  blackUsername?: string;
}
