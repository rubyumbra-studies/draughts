import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ChessmanComponent } from './chessman/chessman.component';
import { ChessBoardComponent } from './chess-board/chess-board.component';

@NgModule({
  declarations: [ChessmanComponent, ChessBoardComponent],
  imports: [BrowserModule, CommonModule],
  exports: [ChessBoardComponent],
  providers: [],
})
export class GameModule {}
