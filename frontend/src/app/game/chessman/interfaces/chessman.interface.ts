export interface Chessman {
  color: 'white' | 'black';
  isKing: boolean;
}
