import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChessmanComponent } from './chessman.component';

describe('ChessmanComponent', () => {
  let component: ChessmanComponent;
  let fixture: ComponentFixture<ChessmanComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChessmanComponent]
    });
    fixture = TestBed.createComponent(ChessmanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
