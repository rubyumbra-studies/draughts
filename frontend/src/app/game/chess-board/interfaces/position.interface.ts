export interface Position {
  rowIndex: number;
  columnIndex: number;
}
