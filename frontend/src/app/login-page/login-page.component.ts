import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';

const LOGIN_VALIDATORS = [
  Validators.required,
  Validators.minLength(4),
  Validators.maxLength(32),
];

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.less'],
})
export class LoginPageComponent {
  form: FormGroup;
  hidePassword = true;
  existingUser = true;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.form = this.fb.group({
      login: ['', [Validators.required]],
      password: ['', Validators.required],
    });
  }

  login() {
    const value = this.form.value;

    if (this.form.valid) {
      this.authService.login(value.login, value.password).subscribe(() => {
        this.router.navigateByUrl('/');
      });
    }
  }

  register() {
    const value = this.form.value;

    if (this.form.valid) {
      this.authService.register(value.login, value.password).subscribe(() => {
        this.router.navigateByUrl('/');
      });
    }
  }

  swapRegisterMode(): void {
    this.form.markAsUntouched();
    this.existingUser = !this.existingUser;

    if (this.existingUser) {
      this.form.controls['login'].setValidators([Validators.required]);
    } else {
      this.form.controls['login'].setValidators([...LOGIN_VALIDATORS]);
    }
  }

  onHideClick(event: Event): void {
    event.stopPropagation();
    this.hidePassword = !this.hidePassword;
  }
}
