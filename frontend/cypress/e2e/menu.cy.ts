import { faker } from '@faker-js/faker';

describe('MENU PAGE', () => {
  let randomName = null;
  let randomPassword = null;

  beforeEach(() => {
    cy.intercept('POST', `/auth/login`).as('login');
    cy.intercept('POST', `/auth/register`).as('register');
    cy.intercept('POST', `/games/create/*`).as('createGame');
    cy.intercept('GET', `/games`).as('listGames');
    cy.intercept('GET', `/users`).as('users');

    cy.visit('/');
    cy.get('[data-e2e-anchor="login:swap-mode"]').click();

    randomName = faker.string.alphanumeric({
      length: { min: 4, max: 10 },
    });
    randomPassword = faker.string.alphanumeric({
      length: { min: 4, max: 12 },
    });

    cy.get('[data-e2e-anchor="login:input-name"]').clear().type(randomName);
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(randomPassword);
    cy.get('[data-e2e-anchor="login:create-button"]').click();
    cy.wait('@register').its('response.statusCode').should('eq', 200);
  });

  it('should create new Train and return back to menu by clicking Home icon', () => {
    cy.visit('/');

    cy.get('[data-e2e-anchor="menu:new-train-button"]')
      .should('contain.text', 'New train')
      .click();

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/train`);
    cy.get('[data-e2e-anchor="board:chessboard"]');

    cy.get('[data-e2e-anchor="header:home-button"]').click();
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/menu`);
  });

  it('should create new Game with other player, check active games in list and re-open previous game by click tile', () => {
    // Creating second user
    cy.get('[data-e2e-anchor="header:logout-button"]').click();
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/login`);

    cy.visit('/');
    cy.get('[data-e2e-anchor="login:swap-mode"]').click();

    const randomName2 = faker.string.alphanumeric({
      length: { min: 4, max: 10 },
    });
    const randomPassword2 = faker.string.alphanumeric({
      length: { min: 4, max: 12 },
    });

    cy.get('[data-e2e-anchor="login:input-name"]').clear().type(randomName2);
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(randomPassword2);
    cy.get('[data-e2e-anchor="login:create-button"]').click();
    cy.wait('@register').its('response.statusCode').should('eq', 200);

    cy.wrap(randomName).should('not.be.null');

    // Create new game
    cy.get('[data-e2e-anchor="menu:new-game-button"]')
      .should('contain.text', 'New game')
      .click();
    cy.get('[data-e2e-anchor="dialog-new-game:select"]').click();
    cy.wait('@users')
      .its('response.statusCode')
      .should('eq', 200)
      .then(() => cy.wait(500));

    cy.get('[data-e2e-anchor="dialog-new-game:option"]')
      .contains(randomName)
      .click();
    cy.get('[data-e2e-anchor="dialog-new-game:create-button"]').click();
    cy.wait('@createGame')
      .its('response.statusCode')
      .should('eq', 200)
      .then(() => cy.wait(500));

    // Return to home
    cy.get('[data-e2e-anchor="header:home-button"]').click();
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/menu`);

    // Check Games list
    cy.get('[data-e2e-anchor="menu:list-games-button"]')
      .should('contain.text', 'Games')
      .click();
    cy.wait('@listGames').its('response.statusCode').should('eq', 200);
    cy.get('[data-e2e-anchor="games-list:white-tile"]').contains(randomName2);
    cy.get('[data-e2e-anchor="games-list:black-tile"]').contains(randomName);
    cy.get('[data-e2e-anchor="games-list:tile"]').click();
  });
});
