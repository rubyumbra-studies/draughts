import { faker } from '@faker-js/faker';

describe('BOARD', () => {
  let randomName = null;
  let randomName2 = null;
  let randomPassword = null;
  let randomPassword2 = null;

  beforeEach(() => {
    cy.intercept('POST', `/auth/login`).as('login');
    cy.intercept('POST', `/auth/register`).as('register');
    cy.intercept('POST', `/games/create/*`).as('createGame');
    cy.intercept('POST', `/actions`).as('newAction');
    cy.intercept('GET', `/games`).as('listGames');
    cy.intercept('GET', `/users`).as('users');

    cy.visit('/');
    cy.get('[data-e2e-anchor="login:swap-mode"]').click();

    randomName = faker.string.alphanumeric({
      length: { min: 4, max: 10 },
    });
    randomPassword = faker.string.alphanumeric({
      length: { min: 4, max: 12 },
    });

    cy.get('[data-e2e-anchor="login:input-name"]').clear().type(randomName);
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(randomPassword);
    cy.get('[data-e2e-anchor="login:create-button"]').click();
    cy.wait('@register').its('response.statusCode').should('eq', 200);
  });

  it('should create new Train, make several moves and destroy white unit', () => {
    cy.visit('/');

    cy.get('[data-e2e-anchor="menu:new-train-button"]')
      .should('contain.text', 'New train')
      .click();

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/train`);
    cy.get('[data-e2e-anchor="board:chessboard"]');

    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Your turn'
    );

    // First player move
    cy.get('[data-e2e-anchor="board:cell"]').eq(78).click();
    cy.get('[data-e2e-anchor="board:cell"]').eq(67).click();
    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Enemy turn'
    );

    // Second player move
    cy.get('[data-e2e-anchor="board:cell"]').eq(27).click();
    cy.get('[data-e2e-anchor="board:cell"]').eq(36).click();
    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Your turn'
    );

    cy.get('[data-e2e-anchor="board:cell"]').eq(67).click();
    cy.get('[data-e2e-anchor="board:cell"]').eq(56).click();

    cy.get('[data-e2e-anchor="board:cell"]').eq(36).click();
    cy.get('[data-e2e-anchor="board:cell"]').eq(47).click();
    cy.get('[data-e2e-anchor="board:chessman"]').should('have.length', 30);

    // Fatal move
    cy.get('[data-e2e-anchor="board:cell"]').eq(56).click();
    cy.get('[data-e2e-anchor="board:cell"]').eq(38).click();
    cy.get('[data-e2e-anchor="board:chessman"]').should('have.length', 29);
  });

  it('should be able to make moves only according to rules', () => {
    cy.visit('/');

    cy.get('[data-e2e-anchor="menu:new-train-button"]')
      .should('contain.text', 'New train')
      .click();

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/train`);
    cy.get('[data-e2e-anchor="board:chessboard"]');

    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Your turn'
    );

    // First player move
    cy.get('[data-e2e-anchor="board:cell"]').eq(76).click();
    cy.get('[data-e2e-anchor="board:cell"]').eq(56).click();

    // State didn't change
    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Your turn'
    );

    cy.get('[data-e2e-anchor="board:cell"]').eq(55).click();
    // State didn't change
    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Your turn'
    );

    cy.get('[data-e2e-anchor="board:cell"]').eq(1).click();
    // State didn't change
    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Your turn'
    );

    cy.get('[data-e2e-anchor="board:cell"]').eq(70).click();
    cy.get('[data-e2e-anchor="board:cell"]').eq(61).click();

    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Enemy turn'
    );
  });

  it('should create new Game with other player and check logging of unit moves', () => {
    // Creating second user
    cy.get('[data-e2e-anchor="header:logout-button"]').click();
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/login`);

    cy.visit('/');
    cy.get('[data-e2e-anchor="login:swap-mode"]').click();

    randomName2 = faker.string.alphanumeric({
      length: { min: 4, max: 10 },
    });
    randomPassword2 = faker.string.alphanumeric({
      length: { min: 4, max: 12 },
    });

    cy.get('[data-e2e-anchor="login:input-name"]').clear().type(randomName2);
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(randomPassword2);
    cy.get('[data-e2e-anchor="login:create-button"]').click();
    cy.wait('@register').its('response.statusCode').should('eq', 200);

    cy.wrap(randomName).should('not.be.null');
    cy.wrap(randomName2).should('not.be.null');

    // Re-login to first user
    cy.get('[data-e2e-anchor="header:logout-button"]').click();
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/login`);

    cy.get('[data-e2e-anchor="login:input-name"]').clear().type(randomName);
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(randomPassword);
    cy.get('[data-e2e-anchor="login:login-button"]').click();
    cy.wait('@login').its('response.statusCode').should('eq', 200);

    // Create new game
    cy.get('[data-e2e-anchor="menu:new-game-button"]')
      .should('contain.text', 'New game')
      .click();
    cy.get('[data-e2e-anchor="dialog-new-game:select"]').click();
    cy.wait('@users')
      .its('response.statusCode')
      .should('eq', 200)
      .then(() => cy.wait(500));

    cy.get('[data-e2e-anchor="dialog-new-game:option"]')
      .contains(randomName2)
      .click();
    cy.get('[data-e2e-anchor="dialog-new-game:create-button"]').click();
    cy.wait('@createGame').its('response.statusCode').should('eq', 200);

    // First player move
    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Your turn'
    );
    cy.get('[data-e2e-anchor="board:cell"]').eq(76).click();
    cy.get('[data-e2e-anchor="board:cell"]').eq(65).click();
    cy.get('[data-e2e-anchor="board:turn-button"]').should(
      'contain.text',
      'Enemy turn'
    );
    cy.wait('@newAction').its('response.statusCode').should('eq', 200);
  });
});
