import { faker } from '@faker-js/faker';

describe('LOGIN PAGE', () => {
  beforeEach(() => {
    cy.intercept('POST', `/auth/register`).as('register');
    cy.intercept('POST', `/auth/login`).as('login');
  });

  it('should redirect to login page for every request until user is signed in', () => {
    cy.visit('/');

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/login`);
    cy.get('[data-e2e-anchor="login:login-button"]').should(
      'have.text',
      'Login'
    );

    cy.visit('/game');
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/login`);

    cy.visit('/test');
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/login`);

    cy.visit('/menu');
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/login`);
  });

  it('should create user and automatically login successfully', () => {
    cy.visit('/');
    cy.get('[data-e2e-anchor="login:swap-mode"]').should(
      'contain.text',
      'Not a member yet?'
    );
    cy.get('[data-e2e-anchor="login:swap-mode"]').click();

    const randomName = faker.string.alphanumeric({
      length: { min: 4, max: 10 },
    });
    const randomPassword = faker.string.alphanumeric({
      length: { min: 4, max: 12 },
    });

    cy.get('[data-e2e-anchor="login:input-name"]').clear().type(randomName);
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(randomPassword);
    cy.get('[data-e2e-anchor="login:create-button"]').click();
    cy.wait('@register').its('response.statusCode').should('eq', 200);
  });

  it('should create user, logout and re-login successfully', () => {
    cy.visit('/');
    cy.get('[data-e2e-anchor="login:swap-mode"]').should(
      'contain.text',
      'Not a member yet?'
    );
    cy.get('[data-e2e-anchor="login:swap-mode"]').click();

    const randomName = faker.string.alphanumeric({
      length: { min: 4, max: 10 },
    });
    const randomPassword = faker.string.alphanumeric({
      length: { min: 4, max: 12 },
    });

    cy.get('[data-e2e-anchor="login:input-name"]').clear().type(randomName);
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(randomPassword);
    cy.get('[data-e2e-anchor="login:create-button"]').click();
    cy.wait('@register').its('response.statusCode').should('eq', 200);
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/menu`);

    cy.get('[data-e2e-anchor="header:logout-button"]').click();
    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/login`);

    // Re-login
    cy.get('[data-e2e-anchor="login:input-name"]').clear().type(randomName);
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(randomPassword);
    cy.get('[data-e2e-anchor="login:login-button"]').click();
    cy.wait('@login').its('response.statusCode').should('eq', 200);
  });

  it('should be forbidden to submit form with invalid fields', () => {
    cy.visit('/');

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}/login`);
    cy.get('[data-e2e-anchor="login:swap-mode"]').click();

    cy.get('[data-e2e-anchor="login:create-button"]').click();
    cy.get('[data-e2e-anchor="login:error-login-required"]').should(
      'contain.text',
      'Login is required'
    );
    cy.get('[data-e2e-anchor="login:error-password-required"]').should(
      'contain.text',
      'Password is required'
    );

    cy.get('[data-e2e-anchor="login:input-name"]')
      .clear()
      .type(
        faker.string.alphanumeric({
          length: 1,
        })
      );
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(
        faker.string.alphanumeric({
          length: 1,
        })
      );
    cy.get('[data-e2e-anchor="login:create-button"]').click();
    cy.get('[data-e2e-anchor="login:error-login-min"]').should(
      'contain.text',
      'Minimum length is 4 characters'
    );

    cy.get('[data-e2e-anchor="login:input-name"]')
      .clear()
      .type(
        faker.string.alphanumeric({
          length: 33,
        })
      );
    cy.get('[data-e2e-anchor="login:input-password"]')
      .clear()
      .type(
        faker.string.alphanumeric({
          length: 33,
        })
      );
    cy.get('[data-e2e-anchor="login:create-button"]').click();
    cy.get('[data-e2e-anchor="login:error-login-max"]').should(
      'contain.text',
      'Maximum length is 32 characters'
    );
  });
});
