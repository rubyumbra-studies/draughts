import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    retries: {
      runMode: 3,
    },
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
