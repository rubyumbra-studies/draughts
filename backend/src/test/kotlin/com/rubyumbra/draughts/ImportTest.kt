package com.rubyumbra.draughts

import com.rubyumbra.draughts.model.Action
import com.rubyumbra.draughts.model.ImportRequest
import com.rubyumbra.draughts.model.ImportResponse
import com.rubyumbra.draughts.model.User
import com.rubyumbra.draughts.repository.ActionRepository
import com.rubyumbra.draughts.repository.GameRepository
import com.rubyumbra.draughts.repository.UserRepository
import com.rubyumbra.draughts.service.ImportService
import org.junit.jupiter.api.Test
import org.mockito.AdditionalAnswers.returnsFirstArg
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.*
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

private const val USER_LOGIN = "user"
private const val USER_PASSWORD = "{noop}pass"
private val USER = User(42, USER_LOGIN, USER_PASSWORD)

@WebMvcTest(ImportService::class)
class ImportTests {
    @MockBean
    private lateinit var restTemplate: RestTemplate

    @MockBean
    private lateinit var users: UserRepository

    @MockBean
    private lateinit var games: GameRepository

    @MockBean
    private lateinit var actions: ActionRepository

    @Autowired
    private lateinit var service: ImportService

    @Test
    fun importTest() {
        val importRequest = ImportRequest("12345678", USER.login, "user2")
        val uri = UriComponentsBuilder.newInstance().apply {
            scheme("https")
            host("lidraughts.org")
            path("/game/export/${importRequest.id}")
            queryParam("moves", "true")
            queryParam("tags", "true")
            queryParam("clocks", "false")
            queryParam("evals", "false")
            queryParam("opening", "false")
            queryParam("literate", "false")
            queryParam("algebraic", "false")
        }.build().encode().toUriString()
        val importResponse = ImportResponse(
            importRequest.id,
            variant = "standard",
            status = null,
            winner = null,
            moves = "32-28 17-22 28x17 11x22",
        )
        val gameId = 1337
        val moves = listOf(
            Action(null, gameId, null, 3, 6, 4, 5, false),
            Action(null, gameId, null, 2, 3, 3, 4, false),
            Action(null, gameId, null, 4, 5, 2, 3, false),
            Action(null, gameId, null, 1, 2, 3, 4, false)
        )

        `when`(
            restTemplate.exchange(
                uri,
                HttpMethod.GET,
                HttpEntity("", HttpHeaders().apply { accept = listOf(MediaType.APPLICATION_JSON) }),
                ImportResponse::class.java
            )
        ).thenReturn(ResponseEntity.ok(importResponse))
        `when`(games.create(USER.id!!, "user2")).thenReturn(gameId)
        `when`(actions.saveAll(argThat<List<Action>> {
            it.size == moves.size && it.zip(moves).all { (actual, expected) ->
                actual.gameId == expected.gameId
                        && actual.fromX == expected.fromX
                        && actual.fromY == expected.fromY
                        && actual.toX == expected.toX
                        && actual.toY == expected.toY
                        && actual.lastTurn == expected.lastTurn
            }
        })).thenAnswer(returnsFirstArg<List<Action>>())

        assert(service.import(USER, importRequest) == gameId)
        verify(games).create(USER.id!!, "user2")
        verify(actions).saveAll(anyCollection())
    }
}
