package com.rubyumbra.draughts

import com.rubyumbra.draughts.controller.UserController
import com.rubyumbra.draughts.service.UserService
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(UserController::class)
@AutoConfigureMockMvc
class SecurityTest {
    @Autowired
    private lateinit var mvc: MockMvc

    @MockBean
    private lateinit var service: UserService

    @Test
    fun unauthorisedUserCantAccessSecuredPage() {
        mvc.perform(get("/users")).andExpect(status().isUnauthorized)
    }

    @Test
    @WithMockUser(username = "user", password = "pass")
    fun authorisedUserCanAccessSecuredPage() {
        `when`(service.users()).thenReturn(listOf())
        mvc.perform(get("/users")).andExpect(status().isOk())
    }
}
