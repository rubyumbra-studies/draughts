create table if not exists users
(
    id       serial
        primary key,
    login    varchar(32) not null
        unique,
    password varchar(32) not null
);

create table if not exists user_statistics
(
    id         integer
        primary key
        references users,
    registered timestamp default CURRENT_TIMESTAMP,
    timestamp  timestamp default CURRENT_TIMESTAMP
);

create table if not exists games
(
    id        serial
        primary key,
    timestamp timestamp default CURRENT_TIMESTAMP,
    white_id  integer
        references users,
    black_id  integer
        references users
);

create table if not exists actions
(
    id        serial
        primary key,
    game_id   integer
        references games,
    timestamp timestamp default CURRENT_TIMESTAMP,
    from_x    integer
        constraint valid_from_x
            check ((0 <= from_x) AND (from_x < 10)),
    from_y    integer
        constraint valid_from_y
            check ((0 <= from_y) AND (from_y < 10)),
    to_x      integer
        constraint valid_to_x
            check ((0 <= to_x) AND (to_x < 10)),
    to_y      integer
        constraint valid_to_y
            check ((0 <= to_y) AND (to_y < 10)),
    last_turn boolean
);

insert into users (login, password)
values ('prometheus', 'prometheus'),
       ('admin', 'admin'),
       ('user', 'user')
on conflict do nothing;

insert into user_statistics (id)
select u.id
from users u
on conflict do nothing;
