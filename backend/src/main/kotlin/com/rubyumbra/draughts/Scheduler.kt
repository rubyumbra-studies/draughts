package com.rubyumbra.draughts

import com.rubyumbra.draughts.repository.UserStatisticsRepository
import io.micrometer.prometheus.PrometheusMeterRegistry
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.concurrent.atomic.AtomicInteger

@Component
class Scheduler(
    meterRegistry: PrometheusMeterRegistry,
    private val statistics: UserStatisticsRepository,
) {
    val registeredAllTime: AtomicInteger = meterRegistry.gauge("registered_all_time", AtomicInteger(0))!!
    val registeredLastMonth: AtomicInteger = meterRegistry.gauge("registered_last_month", AtomicInteger(0))!!
    val registeredLastDay: AtomicInteger = meterRegistry.gauge("registered_last_day", AtomicInteger(0))!!

    val onlineLastMonth: AtomicInteger = meterRegistry.gauge("online_last_month", AtomicInteger(0))!!
    val onlineLastDay: AtomicInteger = meterRegistry.gauge("online_last_day", AtomicInteger(0))!!
    val onlineLastHour: AtomicInteger = meterRegistry.gauge("online_last_hour", AtomicInteger(0))!!
    val onlineLastMinute: AtomicInteger = meterRegistry.gauge("online_last_minute", AtomicInteger(0))!!

    @Scheduled(fixedRate = 1000)
    fun schedulingTask() {
        registeredAllTime.set(statistics.registeredAllTime())
        registeredLastMonth.set(statistics.registeredLastMonth())
        registeredLastDay.set(statistics.registeredLastDay())

        onlineLastMonth.set(statistics.onlineLastMonth())
        onlineLastDay.set(statistics.onlineLastDay())
        onlineLastHour.set(statistics.onlineLastHour())
        onlineLastMinute.set(statistics.onlineLastMinute())
    }
}
