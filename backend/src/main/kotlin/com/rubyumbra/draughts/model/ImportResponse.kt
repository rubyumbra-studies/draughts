package com.rubyumbra.draughts.model

data class ImportResponse(
    val id: String?,
    val variant: String?,
    val status: String?,
    val winner: String?,
    val moves: String?,
)
