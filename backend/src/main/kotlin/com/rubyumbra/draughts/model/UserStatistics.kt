package com.rubyumbra.draughts.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime

@Table("user_statistics")
data class UserStatistics(
    @Id
    var id: Int?,

    val registered: LocalDateTime?,

    val timestamp: LocalDateTime?,
)
