package com.rubyumbra.draughts.model

import io.swagger.v3.oas.annotations.media.Schema
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime

@Table("actions")
@Schema(description = "Action data")
data class Action(
    @Id
    @field:Schema
    var id: Int?,

    @field:Schema(description = "Id of linked game")
    val gameId: Int,

    @field:Schema(description = "Creation time")
    val timestamp: LocalDateTime?,

    @field:Schema(
        description = "Starting position on board by x axis",
        minimum = "0",
        maximum = "9",
    )
    val fromX: Int,

    @field:Schema(
        description = "Starting position on board by y axis",
        minimum = "0",
        maximum = "9",
    )
    val fromY: Int,

    @field:Schema(
        description = "Finishing position on board by x axis",
        minimum = "0",
        maximum = "9",
    )
    val toX: Int,

    @field:Schema(
        description = "Finishing position on board by y axis",
        minimum = "0",
        maximum = "9",
    )
    val toY: Int,

    @field:Schema(description = "Indicate whether player can continue the turn or not")
    val lastTurn: Boolean,
)
