package com.rubyumbra.draughts.model

import io.swagger.v3.oas.annotations.media.Schema
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime


@Table("games")
@Schema(description = "Game data")
data class Game(
    @Id
    @field:Schema
    var id: Int?,

    @field:Schema(description = "Game start time")
    val timestamp: LocalDateTime?,

    @field:Schema(description = "White player id")
    val whiteId: Int,

    @field:Schema(description = "Black player id")
    val blackId: Int,
)
