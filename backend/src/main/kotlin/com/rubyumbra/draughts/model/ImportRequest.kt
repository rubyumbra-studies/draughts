package com.rubyumbra.draughts.model

import io.swagger.v3.oas.annotations.media.Schema

@Schema(description = "Import request data")
data class ImportRequest(
    @field:Schema(
        minLength = 8,
        maxLength = 8,
    )
    val id: String,

    @field:Schema(
        description = "White player login",
        minLength = 4,
        maxLength = 32,
    )
    val white: String,

    @field:Schema(
        description = "Black player login",
        minLength = 4,
        maxLength = 32,
    )
    val black: String,
)
