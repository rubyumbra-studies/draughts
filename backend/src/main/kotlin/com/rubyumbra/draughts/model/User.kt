package com.rubyumbra.draughts.model

import io.swagger.v3.oas.annotations.Hidden
import io.swagger.v3.oas.annotations.media.Schema
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

@Table("users")
@Schema(description = "User data")
data class User(
    @Id
    @field:Schema
    var id: Int?,

    @field:Schema(
        minLength = 4,
        maxLength = 32,
    )
    val login: String,

    @field:Schema(
        minLength = 4,
        maxLength = 32,
    )
    private val password: String?,
) : UserDetails {
    @Hidden
    override fun getAuthorities(): Collection<GrantedAuthority> = listOf()

    override fun getPassword(): String? = password

    @Hidden
    override fun getUsername(): String = login

    @Hidden
    override fun isAccountNonExpired(): Boolean = true

    @Hidden
    override fun isAccountNonLocked(): Boolean = true

    @Hidden
    override fun isCredentialsNonExpired(): Boolean = true

    @Hidden
    override fun isEnabled(): Boolean = true
}
