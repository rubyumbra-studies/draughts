package com.rubyumbra.draughts.service

import com.rubyumbra.draughts.model.Action
import com.rubyumbra.draughts.repository.ActionRepository
import org.springframework.stereotype.Service

@Service
class ActionService(private val repository: ActionRepository) {
    fun actions(gameId: Int): List<Action> = repository.findAllByGameId(gameId)

    fun process(action: Action): Action? = when {
        action.id != null -> null
        action.fromX !in 0..9 -> null
        action.fromY !in 0..9 -> null
        action.toX !in 0..9 -> null
        action.toY !in 0..9 -> null
        else -> repository.save(action)
    }

    fun actionsAfter(gameId: Int, actionId: Int): List<Action> = repository.findAllByGameIdAfter(gameId, actionId)
}
