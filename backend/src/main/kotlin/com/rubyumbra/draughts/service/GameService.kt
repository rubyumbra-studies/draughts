package com.rubyumbra.draughts.service

import com.rubyumbra.draughts.model.Game
import com.rubyumbra.draughts.repository.GameRepository
import org.springframework.stereotype.Service

@Service
class GameService(private val repository: GameRepository) {
    fun games(id: Int): List<Game> = repository.findAllByUserId(id)

    fun create(id: Int, login: String): Int? = repository.create(id, login)
}
