package com.rubyumbra.draughts.service

import com.rubyumbra.draughts.model.Action
import com.rubyumbra.draughts.model.ImportRequest
import com.rubyumbra.draughts.model.ImportResponse
import com.rubyumbra.draughts.model.User
import com.rubyumbra.draughts.repository.ActionRepository
import com.rubyumbra.draughts.repository.GameRepository
import com.rubyumbra.draughts.repository.UserRepository
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod.GET
import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import java.time.LocalDateTime

@Service
class ImportService(
    private val users: UserRepository,
    private val games: GameRepository,
    private val actions: ActionRepository,
    private val restTemplate: RestTemplate,
) {
    private val uriBuilder = UriComponentsBuilder.newInstance().apply {
        scheme("https")
        host("lidraughts.org")
        path("/game/export/{id}")
        queryParam("moves", "true")
        queryParam("tags", "true")
        queryParam("clocks", "false")
        queryParam("evals", "false")
        queryParam("opening", "false")
        queryParam("literate", "false")
        queryParam("algebraic", "false")
    }

    private fun Int.toCell(): Pair<Int, Int> {
        val shifted = this - 1
        val y = shifted / 5
        val x = (shifted * 2 + (1 - y % 2)) % 10
        return y to x
    }

    private fun String.toCell(): Pair<Int, Int>? = this.toIntOrNull()?.toCell()

    private fun moveStringToAction(raw: String): Action? {
        val parts: List<String> = raw.split(Regex("[-x]"))
        if (parts.size != 2) {
            return null
        }
        val (from, to) = parts.map { it.toCell() }
        if (from == null || to == null) {
            return null
        }
        return Action(
            id = null,
            gameId = 0,
            timestamp = LocalDateTime.now(),
            fromX = from.second,
            fromY = from.first,
            toX = to.second,
            toY = to.first,
            lastTurn = false
        )
    }

    fun import(user: User, req: ImportRequest): Int? {
        val url = uriBuilder.buildAndExpand(req.id).encode().toUriString()
        val headers = HttpHeaders().apply { accept = listOf(APPLICATION_JSON) }
        val entity = HttpEntity("", headers)
        val response: ResponseEntity<ImportResponse> =
            restTemplate.exchange(url, GET, entity, ImportResponse::class.java)
        if (response.statusCode != OK) {
            return null
        }
        val body = response.body
        if (body == null || body.id != req.id || body.variant != "standard" || body.moves == null) {
            return null
        }
        val finished = body.winner != null || body.status in listOf("draw", "resign")
        val moves = body.moves.split(Regex("\\s")).map { moveStringToAction(it) ?: return null }
        val userId: Int = user.id ?: users.findByLoginCensored(user.login)?.id ?: return null
        val gameId: Int =
            if (user.login == req.white) games.create(userId, req.black) ?: return null
            else games.create(req.white, userId) ?: return null
        val movesWithGameId = moves.map { it.copy(gameId = gameId) }.toMutableList()
        if (finished) {
            movesWithGameId.add(movesWithGameId.removeLast().copy(lastTurn = true))
        }
        actions.saveAll(movesWithGameId)
        return gameId
    }
}
