package com.rubyumbra.draughts.service

import com.rubyumbra.draughts.DraughtsApplication
import com.rubyumbra.draughts.model.User
import com.rubyumbra.draughts.repository.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserService(private val repository: UserRepository) : UserDetailsService {
    private val log: Logger = LoggerFactory.getLogger(DraughtsApplication::class.java)

    fun users(): List<User> = repository.findAllCensored()

    fun create(user: User): User? = when {
        repository.findByLoginCensored(user.login) != null -> null
        else -> {
            log.info("registered new user: '${user.login}' [${user.id}]")
            repository.save(user)
        }
    }

    fun exists(user: User): Boolean = when {
        user.password == null -> false
        repository.exists(user.login, user.password!!) -> true
        else -> false
    }

    fun user(login: String): User? {
        return repository.findByLoginCensored(login) ?: user(login.toIntOrNull() ?: return null)
    }

    fun user(id: Int): User? = repository.findByIdCensored(id)

    override fun loadUserByUsername(username: String?): UserDetails {
        val ud = repository.findByLogin(username ?: throw UsernameNotFoundException("Username is null"))
            ?: throw UsernameNotFoundException("No such username: '$username'")
        return User(ud.id, ud.login, "{noop}${ud.password}")
    }
}
