package com.rubyumbra.draughts.service

import com.rubyumbra.draughts.model.User
import com.rubyumbra.draughts.repository.UserStatisticsRepository
import org.springframework.stereotype.Service

@Service
class UserStatisticsService(private val statistics: UserStatisticsRepository) {
    fun register(user: User) = if (user.id != null) statistics.register(user.id!!) else Unit

    fun updateOnline(user: User) = when {
        user.id != null -> statistics.updateOnline(user.id)
        else -> statistics.updateOnline(user.login)
    }
}
