package com.rubyumbra.draughts.controller

import com.rubyumbra.draughts.model.Game
import com.rubyumbra.draughts.model.User
import com.rubyumbra.draughts.service.GameService
import com.rubyumbra.draughts.service.UserStatisticsService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/games")
@Tag(name = "Games", description = "Methods to work with games")
class GameController(
    private val service: GameService,
    private val statistics: UserStatisticsService,
) {
    @GetMapping
    @Operation(summary = "Get list of all games of current logged in user")
    fun games(@AuthenticationPrincipal user: User): List<Game> {
        statistics.updateOnline(user)
        return service.games(user.id!!)
    }

    @PostMapping("/create/{login}")
    @Operation(summary = "Create new game for current logged in user and user specified in path")
    fun create(@AuthenticationPrincipal user: User, @Parameter @PathVariable login: String): Int? {
        statistics.updateOnline(user)
        return service.create(user.id!!, login)
    }
}
