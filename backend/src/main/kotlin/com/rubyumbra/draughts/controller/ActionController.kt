package com.rubyumbra.draughts.controller

import com.rubyumbra.draughts.model.Action
import com.rubyumbra.draughts.model.User
import com.rubyumbra.draughts.service.ActionService
import com.rubyumbra.draughts.service.UserStatisticsService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/actions")
@Tag(name = "Actions", description = "Methods to work with actions")
class ActionController(
    private val service: ActionService,
    private val statistics: UserStatisticsService,
) {
    @GetMapping("/{gameId}")
    @Operation(summary = "Get list of all actions of game specified by gameId")
    fun actions(@AuthenticationPrincipal user: User, @Parameter @PathVariable gameId: Int): List<Action> {
        statistics.updateOnline(user)
        return service.actions(gameId)
    }

    @PostMapping
    @Operation(summary = "Add new action (game id specified in action)")
    fun process(@AuthenticationPrincipal user: User, @RequestBody action: Action): ResponseEntity<HttpStatus> {
        statistics.updateOnline(user)
        return ResponseEntity.status(if (service.process(action) == null) HttpStatus.BAD_REQUEST else HttpStatus.OK)
            .build()
    }

    @GetMapping("/{gameId}/{actionId}")
    @Operation(summary = "Get list of actions of game specified by gameId which comes after action specified by actionId")
    fun actionsAfter(
        @AuthenticationPrincipal user: User,
        @Parameter @PathVariable gameId: Int,
        @Parameter @PathVariable actionId: Int
    ): List<Action> {
        statistics.updateOnline(user)
        return service.actionsAfter(gameId, actionId)
    }
}
