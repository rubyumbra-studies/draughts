package com.rubyumbra.draughts.controller

import com.rubyumbra.draughts.model.User
import com.rubyumbra.draughts.service.UserService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/users")
@Tag(name = "Users", description = "Methods to work with users")
class UserController(private val service: UserService) {
    @GetMapping
    @Operation(summary = "Get list of all users")
    fun users(): List<User> = service.users()

    @GetMapping("/{login}")
    @Operation(summary = "Get user by login")
    fun get(@Parameter @PathVariable login: String): User? = service.user(login)
}
