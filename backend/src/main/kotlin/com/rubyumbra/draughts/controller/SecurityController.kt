package com.rubyumbra.draughts.controller

import com.rubyumbra.draughts.model.User
import com.rubyumbra.draughts.service.UserService
import com.rubyumbra.draughts.service.UserStatisticsService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/auth")
@Tag(name = "Security", description = "Methods for authorization")
class SecurityController(
    private val service: UserService,
    private val statistics: UserStatisticsService,
) {
    @PostMapping("/register")
    @Operation(summary = "Create new user")
    @ApiResponse(responseCode = "200", description = "User created")
    @ApiResponse(responseCode = "409", description = "Login already in use")
    fun register(@RequestBody user: User): ResponseEntity<HttpStatus> {
        val created = service.create(user)
        val status = if (created != null) {
            statistics.register(created)
            HttpStatus.OK
        } else HttpStatus.CONFLICT
        return ResponseEntity.status(status).build()
    }

    @PostMapping("/login")
    @Operation(summary = "Sign in check")
    @ApiResponse(responseCode = "200")
    @ApiResponse(responseCode = "401")
    fun login(@RequestBody user: User): ResponseEntity<HttpStatus> {
        val status = if (service.exists(user)) {
            statistics.updateOnline(user)
            HttpStatus.OK
        } else HttpStatus.UNAUTHORIZED
        return ResponseEntity.status(status).build()
    }
}
