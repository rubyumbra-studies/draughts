package com.rubyumbra.draughts.controller

import com.rubyumbra.draughts.model.ImportRequest
import com.rubyumbra.draughts.model.User
import com.rubyumbra.draughts.service.ImportService
import com.rubyumbra.draughts.service.UserStatisticsService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/import")
@Tag(name = "Import", description = "Methods for games import")
class ImportController(
    private val service: ImportService,
    private val statistics: UserStatisticsService,
) {
    @PostMapping
    @Operation(summary = "Import game")
    @ApiResponse(responseCode = "200", description = "Game imported")
    @ApiResponse(responseCode = "405", description = "Signed in player not in players")
    @ApiResponse(responseCode = "400", description = "Game not found")
    fun import(@AuthenticationPrincipal user: User, @RequestBody req: ImportRequest): ResponseEntity<Int?> {
        statistics.updateOnline(user)
        return when {
            user.login != req.white && user.login != req.black -> ResponseEntity(null, HttpStatus.METHOD_NOT_ALLOWED)
            else -> when (val gameId: Int? = service.import(user, req)) {
                null -> ResponseEntity(null, HttpStatus.BAD_REQUEST)
                else -> ResponseEntity(gameId, HttpStatus.OK)
            }
        }
    }
}
