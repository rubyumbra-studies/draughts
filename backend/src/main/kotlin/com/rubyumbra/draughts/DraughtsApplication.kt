package com.rubyumbra.draughts

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class DraughtsApplication

fun main(args: Array<String>) {
    runApplication<DraughtsApplication>(*args)
}
