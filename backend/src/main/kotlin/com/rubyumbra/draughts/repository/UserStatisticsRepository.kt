package com.rubyumbra.draughts.repository

import com.rubyumbra.draughts.model.UserStatistics
import org.springframework.data.jdbc.repository.query.Modifying
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface UserStatisticsRepository : CrudRepository<UserStatistics, Int> {
    @Modifying
    @Query("insert into user_statistics (id) values (:id) on conflict do nothing")
    fun register(@Param("id") id: Int)

    @Modifying
    @Query("update user_statistics set timestamp = CURRENT_TIMESTAMP where id = :id")
    fun updateOnline(@Param("id") id: Int?)

    @Modifying
    @Query("update user_statistics s set timestamp = CURRENT_TIMESTAMP from users u where u.login = :login AND s.id = u.id")
    fun updateOnline(@Param("login") login: String)

    @Query("select count(*) from user_statistics")
    fun registeredAllTime(): Int

    @Query("select count(*) from user_statistics s where s.registered > CURRENT_TIMESTAMP - '1 month'::interval")
    fun registeredLastMonth(): Int

    @Query("select count(*) from user_statistics s where s.registered > CURRENT_TIMESTAMP - '1 day'::interval")
    fun registeredLastDay(): Int

    @Query("select count(*) from user_statistics s where s.timestamp > CURRENT_TIMESTAMP - '1 month'::interval")
    fun onlineLastMonth(): Int

    @Query("select count(*) from user_statistics s where s.timestamp > CURRENT_TIMESTAMP - '1 day'::interval")
    fun onlineLastDay(): Int

    @Query("select count(*) from user_statistics s where s.timestamp > CURRENT_TIMESTAMP - '1 hour'::interval")
    fun onlineLastHour(): Int

    @Query("select count(*) from user_statistics s where s.timestamp > CURRENT_TIMESTAMP - '1 minute'::interval")
    fun onlineLastMinute(): Int
}
