package com.rubyumbra.draughts.repository

import com.rubyumbra.draughts.model.Game
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface GameRepository : CrudRepository<Game, Int> {
    @Query("select * from games as g where g.white_id = :id or g.black_id = :id")
    fun findAllByUserId(@Param("id") id: Int): List<Game>

    @Query("insert into games(white_id, black_id) (select :id, u.id from users u where u.login = :login) returning id")
    fun create(@Param("id") id: Int, @Param("login") login: String): Int?

    @Query("insert into games(white_id, black_id) (select u.id, :id from users u where u.login = :login) returning id")
    fun create(@Param("login") login: String, @Param("id") id: Int): Int?
}
