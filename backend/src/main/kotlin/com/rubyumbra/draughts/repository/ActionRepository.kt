package com.rubyumbra.draughts.repository

import com.rubyumbra.draughts.model.Action
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface ActionRepository : CrudRepository<Action, Int> {
    @Query("select * from actions a where a.game_id = :game_id")
    fun findAllByGameId(@Param("game_id") gameId: Int): List<Action>

    @Query("select * from actions a where a.game_id = :game_id and a.id > :id")
    fun findAllByGameIdAfter(@Param("game_id") gameId: Int, @Param("id") id: Int): List<Action>
}
