package com.rubyumbra.draughts.repository

import com.rubyumbra.draughts.model.User
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface UserRepository : CrudRepository<User, Int> {
    @Query("select u.id, u.login, null from users as u where u.id = :id")
    fun findByIdCensored(@Param("id") id: Int): User?

    @Query("select u.id, u.login, null from users as u")
    fun findAllCensored(): List<User>

    @Query("select u.id, u.login, null from users as u where u.login = :login")
    fun findByLoginCensored(@Param("login") login: String): User?

    @Query("select * from users as u where u.login = :login")
    fun findByLogin(@Param("login") login: String): User?

    @Query("select count(*) = 1 from users u where u.login = :login and u.password = :password")
    fun exists(@Param("login") login: String, @Param("password") password: String): Boolean
}
